
// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
// and what to do when importing types
declare namespace App {
	interface Platform {
		env: {
			DISCORD_ID;
			DISCORD_SECRET;
			AUTH_SECRET;
			AUTH_TRUST_HOST: true;
		};
		context: {
			waitUntil(promise: Promise<any>): void;
		};
		caches: CacheStorage & { default: Cache }
	}
	// interface PageData {}
	// interface Error {}
	// interface Platform {}
	import 'unplugin-icons/types/svelte'
}
