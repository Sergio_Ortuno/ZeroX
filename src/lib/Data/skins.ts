export const skins = [
    {
        Driver: [
            {
                img: "./skins/Piloto/piloto.webp",
                car: 'Piloto',
                skin: 'https://www.tradingpaints.com/showroom/view/681467/Driver-Suit-v2'
            }
        ],

        LMP: [
            {
                img: "./skins/LMP/cadi_gtp.webp",
                car: 'Cadillac V-Series.R GTP',
                skin: 'https://www.tradingpaints.com/showroom/view/727266/Cadillac-VSeriesR-GTP-v1'
            },

            {
                img: "./skins/LMP/p217.webp",
                car: 'Dallara P217',
                skin: 'https://www.tradingpaints.com/showroom/view/737046/Dallara-P217-LMP2-v2'
            },
        ],

        GTE: [
            {
                img: "./skins/GTE/bmw.webp",
                car: 'BMW M8 GTE',
                skin: 'https://www.tradingpaints.com/showroom/view/679837/BMW-M8-GTE-SN-Version2024'
            },

            {
                img: "./skins/GTE/corvette.webp",
                car: 'Chevrolet Corvette C8.R GTE',
                skin: 'https://www.tradingpaints.com/showroom/view/699975/Chevrolet-Corvette-C8-GTE-SN-v2'
            }
        ],

        GT3: [
            {
                img: "./skins/GT3/ferrari.webp",
                car: 'Ferrari 296 GT3',
                skin: 'https://www.tradingpaints.com/showroom/view/681474/Ferrari-296-GT3-v2-SN'
            },

            {
                img: "./skins/GT3/porsche.webp",
                car: 'Porsche 911 GT3 R (992)',
                skin: 'https://www.tradingpaints.com/showroom/view/679839/Porsche-992r-GT3-SN-Version2024'
            },

            {
                img: "./skins/GT3/bmw.webp",
                car: 'BMW M4 GT3',
                skin: 'https://www.tradingpaints.com/showroom/view/679834/BMW-M4-GT3-SN-version2024'
            },

            {
                img: "./skins/GT3/mercedes.webp",
                car: 'Mercedes-AMG GT3',
                skin: 'https://www.tradingpaints.com/showroom/view/681471/Mercedes-AMG-GT3-2020-v2-SN'
            },

            {
                img: "./skins/GT3/audi.webp",
                car: 'Audi R8 LMS EVO II GT3',
                skin: 'https://www.tradingpaints.com/showroom/view/699690/AudiR8EVO-SN-v2'
            },

            {
                img: "./skins/GT3/lambo.webp",
                car: 'Lamborghini Huracan GT3 EVO',
                skin: 'https://www.tradingpaints.com/showroom/view/735340/Lamborghini-Huracan-GT3-EVO'
            }
        ],

        GT4: [
            {
                img: "./skins/GT4/mercedes.webp",
                car: 'Mercedes-AMG GT4',
                skin: 'https://www.tradingpaints.com/showroom/view/699979/Mercedes-AMG-GT4-SN-v2'
            },
            {
                img: "./skins/GT4/bmw.webp",
                car: 'BMW M4 GT4',
                skin: 'https://www.tradingpaints.com/showroom/view/737049/BMW-M4-GT4'
            }
        ],

        Monoplazas: [
            {
                img: "./skins/Monoplazas/f4.webp",
                car: 'FIA F4',
                skin: 'https://www.tradingpaints.com/showroom/view/699992/FIA-F4-SN-v2'
            },

            {
                img: "./skins/Monoplazas/f3.webp",
                car: 'Dallara F3',
                skin: 'https://www.tradingpaints.com/showroom/view/735339/Dallara-F3-v2'
            },

            {
                img: "./skins/Monoplazas/vee.webp",
                car: 'Formula Vee',
                skin: 'https://www.tradingpaints.com/showroom/view/735335/Formula-Vee'
            },

            {
                img: "./skins/Monoplazas/fford.webp",
                car: 'Ray FF1600',
                skin: 'https://www.tradingpaints.com/showroom/view/737179/Ray-FF1600'
            }
        ],

        Oval: [
            {
                img: "./skins/Oval/chevyA.webp",
                car: 'NASCAR Next Gen Camaro ZL1',
                skin: 'https://www.tradingpaints.com/showroom/view/727410/NASCAR-Next-Gen-Camaro-ZL1-v1'
            },

            {
                img: "./skins/Oval/chevyB.webp",
                car: 'NASCAR XFINITY Chevrolet Camaro',
                skin: 'https://www.tradingpaints.com/showroom/view/727414/NASCAR-XFINITY-Chevrolet-Camaro-v1'
            }
        ],

        Otros: [
            {
                img: "./skins/Otros/mx5.webp",
                car: 'Mazda MX5',
                skin: 'https://www.tradingpaints.com/showroom/view/681475/Mazda-MX5-2016-v2-SN'
            },

            {
                img: "./skins/Otros/pcup.webp",
                car: 'Porsche 911 GT3 Cup (992)',
                skin: 'https://www.tradingpaints.com/showroom/view/699974/Porsche-911-GT3-Cup-992-SN-v2'
            },

            {
                img: "./skins/Otros/gr86.webp",
                car: 'Toyota GR86',
                skin: 'https://www.tradingpaints.com/showroom/view/738565/Toyota-GR86'
            }
        ]
    }
];
