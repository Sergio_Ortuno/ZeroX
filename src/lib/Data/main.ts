export const main = [
  {
    title: 'Quienes somos',
    img: "./main/quien.webp",
    info: '<p>ZeroX Racing Team es un equipo de <a href="https://www.iracing.com/">iRacing</a> que nace en 2021 con el simple objetivo de pasarlo bien, disfrutar de las carreras de resistencia y competir en diversos campeonatos por equipos e individuales.</p></br><p>Somos un equipo pequeño pero con muy buen ambiente donde ademas de compartir ideas para mejorar, entrenar juntos y pasarlo bien en iRacing, tambien hablamos de otras cosas, sobre todo de carreras de varias disciplinas (F1, IMSA, WEC, Indycar..)</p></br><p>Nuestra filosofia es bien sencilla. Disfruta de las carreras y pasalo genial con los compañeros.</p>',
  },

  {
    title: 'Presentamos nuestra web!',
    img: "./main/kermit.webp",
    info: '<p>Esta web nace como un proyecto personal con el fin de tener un lugar donde poder contar nuestras aventuras, anecdotas y experiences en los diversos campeonatos o eventos que vayamos participando como equipo, asi como anecdotas o experiencias individuales de nuestros pilotos.</p> </br> <p>A dia de hoy, la pagina tiene algunas funcionalidades en desarrollo, como seria el inicio de sesion (solo para pilotos) o la seccion eventos, la cual sera un espacio donde ver los distintos eventos del equipo, junto a un calendario que indique las fechas y pilotos que participan en dicho evento.</p>',
  },

  {
    title: 'Campeonato individual K-Series',
    img: "./main/Logo_k-series.webp",
    info: '<p>Primer torneo en el que participaremos despues del lavado de imagen de cara a 2024!</p></br><p>Este torneo individual esta organizado por nuestros amigos de <a href="https://twitter.com/ktd_competicion">KTD Competicion.</a></p></br><p>Participaran 40 pilotos a lo largo de 6 carreras (lunes cada dos semanas) y tiene una limitacion de IR de 1.5k - 3k</p></br><p>Deseamos mucha suerte a Teo, Alfon y Sergio, nuestros 3 pilotos participantes!</p>',
  },

];
