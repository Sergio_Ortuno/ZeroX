export const drivers = [
  {
    name: "Sergio",
    img: "./drivers/M8.webp",
    info: "<p>Competidor de GTs, tanto en monoclase como multiclase.</p></br><p>Siempre dispuesto a correr resistencias, ya sean 4h, 6h, 12h o 24h. El objetivo es disfrutar y pasar un rato agradable y divertido con los compañeros de equipo.</p></br><p>El coche va perfecto.</p> ",
    rol: "Co-Fundador"
  },

  {
    name: "Alfon",
    img: "./drivers/cadi.webp",
    info: "<p>Piloto con mas ganas de correr que tiempo para hacerlo, aunque en las resistencias, suelo estar en casi todas.</p></br><p>Prototipo en carrera multiclase es la combinación que más me gusta, pero se me puede ver en alguna carrera con GT3 o incluso algún monoplaza.</p></br><p>Lo que me falta de ritmo lo intento compensar haciendo grupo, muy fan de las risas que nos echamos en las resistencias.</p>",
    rol: "Co-Fundador"
  },

  {
    name: "Javi",
    img: "./drivers/f3.webp",
    info: "<p>Me gusta correr todo tipo de monoplazas, principalmente F4 y F3 ya que las sensaciones son unicas.</p></br><p>Siempre dispuesto a competir para pasarlo bien y echar unas risas entra compañeros.</p></br><p>Nos vemos en la pista!</p>",
    rol: "Co-Fundador"
  },

  {
    name: "Teo",
    img: "./drivers/cadi.webp",
    info: "<p>Posiblemente el piloto mas humilde y talentoso del equipo.</p></br><p>No tiene 10k de IR porque no le apetece.</p> ",
    rol: "Co-Fundador"
  },

  {
    name: "Arturo",
    img: "./drivers/mx5.webp",
    info: "<p>Con rebufo soy pro y sin el tengo un g29.</p> ",
    rol: "Piloto"
  },

  {
    name: "Esteban",
    img: "./drivers/M8.webp",
    info: "<p>Me gustan los GTE, GT3, los formulas y vender la adoras los domingos.</p>",
    rol: "Piloto"
  },

  {
    name: "Pau",
    img: "./drivers/mercedes-gt3.webp",
    info: "<p>Piloto con gran experiencia en GT3, con demasiado caracter pero muy justo sobre la pista.</p></br><p>Mis adversarios tiemblan al ver mi nombre en el split.</p>",
    rol: "Piloto"
  },

  {
    name: "Juanfu",
    img: "./drivers/M4.webp",
    info: "<p>Piloto novato, apasionado de los GT.</p></br><p>Con muchisimas ganas de aprender circuitos nuevos, conocer nuevos amantes de la velocidad, competir y mejorar en pista.</p>",
    rol: "Piloto"
  },

  {
    name: "Adri",
    img: "./drivers/porsche-gt3.webp",
    info: "<p>Piloto principalmente de GT y fan de las carreras de gestión.</p></br><p> Ahorrar combustible y cuidar gomas es lo mio.</p></br><p> Me gusta buscar el límite y quedarme ahí. Si me encuentras en pista voy a ser duro, pero siempre legal.</p>",
    rol: "Piloto"
  },

  {
    name: "Luis Miguel",
    img: "./drivers/ferrari-gt3.webp",
    info: "<p>Crecí entre motos y olor a gasolina. El simracing me permite disfrutar de la competición.</p></br><p>Me podrás encontrar en carreras de GTs, principalmente GT3, ya sea Sprint o Endurance.</p></br><p>Ahora disfrutando de las resistencias con el equipo.</p></br><p>Keep Pushsing!</p>",
    rol: "Piloto"
  },

  {
    name: "Claudi",
    img: "./drivers/radical.webp",
    info: "<p>Desde pequeño estoy enamorado de las carreras de resistencia tanto de motos como de coches y disfruto cuando paso horas al volante de GTs y Prototipos.</p></br><p>iRacing es mi vida y busco mejorar día a día.</p>",
    rol: "Piloto"
  },

  {
    name: "David",
    img: "./drivers/f3.webp",
    info: "<p>Piloto de monoplazas especializado de todas sus categorías aunque especialmente de Skippy y F3.</p></br><p>Si se tercia también puedo correr prototipos y Gt3 con buen ritmo aunque mi cerrazón es ir en coches que no admitan copiloto.</p></br><p>Mi principal objetivo es pasarlo bien e ir mejorando poco a poco junto a mis compañeros.</p></br><p>Nos vemos por el retrovisor!</p>",
    rol: "Piloto"
  },

  {
    name: "Airam",
    img: "./drivers/porsche-cup.webp",
    info: "<p>Me gusta correr con cualquier coche y sin entrenar demasiado, si siento que soy seguro en pista pa'lante.</p></br><p>Lo que mas suelo disfrutar es la Porsche Fixed y la Radical, aunque también me gustan los prototipos y formulas.</p></br><p>Fan de las resistencias, sobretodo si hay buen rollo y sentido del humor.</p>",
    rol: "Piloto"
  },

  {
    name: "Jorge",
    img: "./drivers/f3.webp",
    info: "<p>Soy un piloto que pretende ser competitivo tanto en monoplazas como en GTs.</p></br><p>Me queda mucho que aprender y mejorar, pero si algo me caracteriza, es que soy un piloto limpio y leo bien las carreras.</p></br><p>Siempre dispuesto a unas carreras, unas cervezas y unas buenas risas.</p>",
    rol: "Piloto"
  },

  {
    name: "Santi",
    img: "./drivers/f3.webp",
    info: "<p>Piloto de Karting e intento de Sim Driver.</p>",
    rol: "Piloto"
  },

  {
    name: "Asier",
    img: "./drivers/f4.webp",
    info: "<p>Encantado de correr en equipo, pasarlo bien e intentar acabar lo más arriba posible.</p></br><p>Fan de los fórmulas pero dispuesto a conducir cualquier cosa que tenga ruedas.</p>",
    rol: "Piloto"
  },

  {
    name: "Ruben",
    img: "./drivers/sf.webp",
    info: "<p>Me encanta mirar datos y telemetrias para ver en qué puedo mejorar.</p></br><p>Fanático de los monoplazas y las  resistencias ya sea con GT o prototipos para pasarlo bien con mis compañeros de equipo.</p></br><p>No es cosa de setup</p>",
    rol: "Piloto"
  },

  {
    name: "Nacho",
    img: "./drivers/porsche-gt3.webp",
    info: "<p>Me gusta el Drift , pero aquí no me dejan, así que me toca ser rápido.</p></br><p>Nací con 'Hot wheels' en las manos y aquí sigo, absorbido hasta la médula por esta pasión.</p>",
    rol: "Piloto"
  }
];
