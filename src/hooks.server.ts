// PUBLIC ENV

// import { SvelteKitAuth } from "@auth/sveltekit"
// import Discord from "@auth/core/providers/discord"
// import type { Handle } from "@sveltejs/kit";


// export const handle = SvelteKitAuth(async (event) => {
//   const authOptions = {
//     providers: [Discord({ clientId: event.platform?.env.DISCORD_ID, clientSecret: event.platform?.env.DISCORD_SECRET })],
//     secret: event.platform?.env.AUTH_SECRET,
//     trustHost: true
//   }
//   return authOptions
// }) satisfies Handle;


// LOCAL ENV

import { SvelteKitAuth } from "@auth/sveltekit"
import Discord from "@auth/core/providers/discord"

import {
  DISCORD_ID,
  DISCORD_SECRET
} from "$env/static/private"

export const handle = SvelteKitAuth({
  providers: [
    Discord({ clientId: DISCORD_ID, clientSecret: DISCORD_SECRET })
  ],
})