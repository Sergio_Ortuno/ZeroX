
import type { CustomThemeConfig } from '@skeletonlabs/tw-plugin';

export const myCustomTheme: CustomThemeConfig = {
  name: 'my-custom-theme',
  properties: {
    // =~= Theme Properties =~=
    "--theme-font-family-base": 'HankenGrotesk',
    "--theme-font-family-heading": "LEMONMILK",
    "--theme-font-color-base": "0 0 0",
    "--theme-font-color-dark": "255 255 255",
    "--theme-rounded-base": "9999px",
    "--theme-rounded-container": "8px",
    "--theme-border-base": "1px",
    // =~= Theme On-X Colors =~=
    "--on-primary": "255 255 255",
    "--on-secondary": "255 255 255",
    "--on-tertiary": "255 255 255",
    "--on-success": "255 255 255",
    "--on-warning": "0 0 0",
    "--on-error": "255 255 255",
    "--on-surface": "255 255 255",
    // =~= Theme Colors  =~=
    // primary | #083e39
    "--color-primary-50": "218 226 225", // #dae2e1
    "--color-primary-100": "206 216 215", // #ced8d7
    "--color-primary-200": "193 207 206", // #c1cfce
    "--color-primary-300": "156 178 176", // #9cb2b0
    "--color-primary-400": "82 120 116", // #527874
    "--color-primary-500": "8 62 57", // #083e39
    "--color-primary-600": "7 56 51", // #073833
    "--color-primary-700": "6 47 43", // #062f2b
    "--color-primary-800": "5 37 34", // #052522
    "--color-primary-900": "4 30 28", // #041e1c
    // secondary | #50b795
    "--color-secondary-50": "229 244 239", // #e5f4ef
    "--color-secondary-100": "220 241 234", // #dcf1ea
    "--color-secondary-200": "211 237 229", // #d3ede5
    "--color-secondary-300": "185 226 213", // #b9e2d5
    "--color-secondary-400": "133 205 181", // #85cdb5
    "--color-secondary-500": "80 183 149", // #50b795
    "--color-secondary-600": "72 165 134", // #48a586
    "--color-secondary-700": "60 137 112", // #3c8970
    "--color-secondary-800": "48 110 89", // #306e59
    "--color-secondary-900": "39 90 73", // #275a49
    // tertiary | #5769bf
    "--color-tertiary-50": "230 233 245", // #e6e9f5
    "--color-tertiary-100": "221 225 242", // #dde1f2
    "--color-tertiary-200": "213 218 239", // #d5daef
    "--color-tertiary-300": "188 195 229", // #bcc3e5
    "--color-tertiary-400": "137 150 210", // #8996d2
    "--color-tertiary-500": "87 105 191", // #5769bf
    "--color-tertiary-600": "78 95 172", // #4e5fac
    "--color-tertiary-700": "65 79 143", // #414f8f
    "--color-tertiary-800": "52 63 115", // #343f73
    "--color-tertiary-900": "43 51 94", // #2b335e
    // success | #6a1266
    "--color-success-50": "233 219 232", // #e9dbe8
    "--color-success-100": "225 208 224", // #e1d0e0
    "--color-success-200": "218 196 217", // #dac4d9
    "--color-success-300": "195 160 194", // #c3a0c2
    "--color-success-400": "151 89 148", // #975994
    "--color-success-500": "106 18 102", // #6a1266
    "--color-success-600": "95 16 92", // #5f105c
    "--color-success-700": "80 14 77", // #500e4d
    "--color-success-800": "64 11 61", // #400b3d
    "--color-success-900": "52 9 50", // #340932
    // warning | #db99e4
    "--color-warning-50": "250 240 251", // #faf0fb
    "--color-warning-100": "248 235 250", // #f8ebfa
    "--color-warning-200": "246 230 248", // #f6e6f8
    "--color-warning-300": "241 214 244", // #f1d6f4
    "--color-warning-400": "230 184 236", // #e6b8ec
    "--color-warning-500": "219 153 228", // #db99e4
    "--color-warning-600": "197 138 205", // #c58acd
    "--color-warning-700": "164 115 171", // #a473ab
    "--color-warning-800": "131 92 137", // #835c89
    "--color-warning-900": "107 75 112", // #6b4b70
    // error | #5649e5
    "--color-error-50": "230 228 251", // #e6e4fb
    "--color-error-100": "221 219 250", // #dddbfa
    "--color-error-200": "213 210 249", // #d5d2f9
    "--color-error-300": "187 182 245", // #bbb6f5
    "--color-error-400": "137 128 237", // #8980ed
    "--color-error-500": "86 73 229", // #5649e5
    "--color-error-600": "77 66 206", // #4d42ce
    "--color-error-700": "65 55 172", // #4137ac
    "--color-error-800": "52 44 137", // #342c89
    "--color-error-900": "42 36 112", // #2a2470
    // surface | #09453f
    "--color-surface-50": "218 227 226", // #dae3e2
    "--color-surface-100": "206 218 217", // #cedad9
    "--color-surface-200": "194 209 207", // #c2d1cf
    "--color-surface-300": "157 181 178", // #9db5b2
    "--color-surface-400": "83 125 121", // #537d79
    "--color-surface-500": "9 69 63", // #09453f
    "--color-surface-600": "8 62 57", // #083e39
    "--color-surface-700": "7 52 47", // #07342f
    "--color-surface-800": "5 41 38", // #052926
    "--color-surface-900": "4 34 31", // #04221f

  }
}