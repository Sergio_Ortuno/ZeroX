

# ZeroX Racing Team  

<img alt="Logo" src="static/LogoZX.webp" width="120" />
<h1>Web Page for my iRacing team <a href="https://twitter.com/zeroxracingteam"> ZeroX Racing Team </a>
</br>
I'm creating this website to learn <a href="https://kit.svelte.dev/">SvelteKit</a> and <a href="https://www.skeleton.dev/">Skeleton UI</a>

Visit the website: <a href="https://zerox.pages.dev/" target="_blank">zerox.pages.dev</a></h1>

# Screenshots Desktop

## Main
<img alt="Screenshot Desktop Main Page" src="screenshot/screenshot_main.webp"/>

## Drivers
<img alt="Screenshot Desktop Drivers Page" src="screenshot/screenshot_drivers.webp"/>

# Screenshots Mobile
<img alt="Screenshot Mobile Drivers Page" src="screenshot/screenshot_drivers_mobile.webp"/>

# create-svelte

Everything you need to build a Svelte project, powered by [`create-svelte`](https://github.com/sveltejs/kit/tree/master/packages/create-svelte).

## Creating a project

If you're seeing this, you've probably already done this step. Congrats!

```bash
# create a new project in the current directory
npm create svelte@latest

# create a new project in my-app
npm create svelte@latest my-app
```

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.

# License

Distributed under the GPL-3.0 License. See [LICENSE](LICENSE) for more information.
